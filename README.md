# ordinaPlexEx

Sample project with a framework in order to showcase our basic skills.

To separate the framework from the implementation with the tests
it is put on a separate branch: master-frame.

Developments of the framework are ideally done on the master-frame branch.
When done, the changes are merged into the master branch to make them available.

This will allow, in light of this exercise, to extract the framework later on
while standing on itself. Kind of as a subproject / separte library within this
one repository.

So all you need to get the entire thing is master. master-frame is just the framework.

## General Concepts
### Configuration
There are some parameters to set up for this to work.
The default file that is loaded is called 'selenium.properties' and it should be
placed in the resources folder that can be reached through the ClassLoader.
This implies that it should be placed under src/test/resources when running the
framework from there. This will be the case when you run it as an imported dependency.
When running it straight from the framework project, then it needs to be under
src/main/resources.

For the available parameters, check selenium.properties under src/main/resources.

### Driver Management
The driver is created from a DriverFactory. There is a singleton that stores
a driver per thread that can be used.

The idea is that there may be parallel tests. These parallel tests are done for
a single WebDriver configuration. If there's tests on multiple configurations we
can do 1 of 2 things.

1. Launch the tests multiple times with different configurations
2. Send the tests to capable platforms like BrowserStack that can handle the
different  configurations and can run those in parallel.

The WebDriver is created when calling to open a site with Statics.open(String)
for example.

To put it down, you need to call the Statics.destroy() method. This will call
WebDriver#quit() as well as removing it from the thread map.

### Framework Point of View
The framework tries to live up to these standards:
* Keep to Selenium standards, stay close to the core concepts
* Hide the boilerplate
* Flexible and extendable

## Example
```
package example;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;

import static be.mdi.selenium.simpleseframe.core.Statics.open;
import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class testClass {

    public void testMethod() {
        //Open a page
        //The driver is opened automatically, according to configuration.
        open("http://www.google.com");

        //find an element, returning a WebElementWrapper.
        //WebElementWrapper implements WebElement, so it can be used
        //as a WebElement as well. It also stores the WebElement as
        //well as the locator.
        //The wrapper provides for frame-work specific extentions.
        //Default strategy: css
        WebElementWrapper wew = f("cssValue");

        //To find by xpath, use the Statics#fx(String) method
        WebElementWrapper wew = fx("xpathValue");
        
        //And the core, of course, is the By
        WebElementWrapper wew = f(By.id("someId"));

        //you can chain the items as you would with a WebElement
        f("by some css").fx("by/xpath").click();
    }
}
```