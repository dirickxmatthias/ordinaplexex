package be.mdi.selenium.simpleseframe.frameworktest;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import be.mdi.selenium.simpleseframe.core.Config;

public class ConfigTest {
	
	@Test
	public void copyLocalExampleTest() throws IOException {
		Config.copyLocalExample("target/selenium.local.properties");
	}

}
