package be.mdi.selenium.simpleseframe.frameworktest;

public class PageObject {
	private String param;
	public PageObject() {
		
	}
	PageObject(String param) {
		this.param = param;
	}
	String getParam() {
		return this.param;
	}
}