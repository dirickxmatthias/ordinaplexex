package be.mdi.selenium.simpleseframe.frameworktest;

import be.mdi.selenium.simpleseframe.core.driver.WebDriverType;
import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import be.mdi.selenium.simpleseframe.util.PropertyManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.openqa.selenium.remote.DesiredCapabilities;

import be.mdi.selenium.simpleseframe.core.Config;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;

import static be.mdi.selenium.simpleseframe.core.Statics.*;
import static be.mdi.selenium.simpleseframe.core.Driver.*;

public class WebDriverIntegrationTest {
	
	@BeforeEach
	public void setup() {
		Config.reset();
		
		//Set default driver to html_unit for non-windows to have the test running on the server level.
		if(System.getProperty("os.name").toLowerCase().startsWith("windows")) {
			Config.setup().setDriverType(WebDriverType.CHROME);
		} else {
			Config.setup().setDriverType(WebDriverType.HTML_UNIT);
		}
		System.out.println(Config.getDriverType().toString());
	}
	
	@AfterEach
	public void cleanUp() {
		cleanBrowser();
	}

	@AfterAll
	public static void destory() {
		destroyAllBrowsers();
	}
	
	@Test
	public void weCanNavigateToAnAddress() {
		open("https://www.google.com");
		assertThat(getPageTitle()).isEqualTo("Google");
	}
	
	@Test
	@EnabledOnOs({OS.WINDOWS})
	public void weCanConnectToRemoteDrivers() {
		/*
		 * This test requires a remote hub to be available.
		 * If not it will fail. There should be a permanent
		 * remote hub available in the future in the docker
		 * instance of the server.
		 */
		//Destory driver to free up the slot.
		//The framework assumes worker threads to be launched on the same browser.
		//parallelizeation within the framework is not over browsers, but over tests.
		destory();
		Config.setup()
	        .setDriverType(WebDriverType.REMOTE)
	        .setRemoteHubUrl("http://127.0.0.1:4444/wd/hub");
		
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setBrowserName("chrome");
		caps.setVersion("85");
		Config.setup().setDesiredCapabilities(caps);
		
		open("https://www.google.com");
		
		//restore the setting
		PropertyManager.setProperty("driver", "chrome");
		PropertyManager.setProperty("version", "85");
		
		assertThat(getPageTitle()).isEqualTo("Google");

		//desttroy the browser again for the next standard tests.
		destory();
	}
	
	@Test
	public void weGetThePageObjectFromOpenIfProvided() {
		PageObject po = open("https://www.google.com", PageObject.class, new PageObject("value"));
		assertThat(po.getParam()).isEqualTo("value");
	}
	
	@Test
	public void weGetThePageObjectFromOpenIfProvidedJustClass() {
		PageObject po = open("https://www.google.com", PageObject.class);
		assertThat(po.getParam()).isNullOrEmpty();
	}
	
	@Test
	public void weCanHaveLocalTestsWithProxyWithoutHeaders() {
		Config.setup().setUseProxy(true);
		open("https://www.google.com");
		assertThat(getPageTitle()).isEqualTo("Google");
	}
	
	@Test
	public void weCanHaveLocalTestsWithProxyWithHeaders() throws InterruptedException {
		HashMap<String, String> proxyHeaders = new HashMap<>();
		proxyHeaders.put("header", "value");
		
		Config.setup()
		    .setUseProxy(true)
		    .setProxyHeaders(proxyHeaders);
		
		open("http://www.google.be");
	}
	
	
	@Test
	@EnabledOnOs({OS.WINDOWS})
	public void weCanHaveRemoteTestsWithProxyConnectingToOurAddress() throws InterruptedException {
		HashMap<String, String> proxyHeaders = new HashMap<>();
		proxyHeaders.put("some-header", "a value for the header");

		destroy();
		Config.setup()
		    .setDriverType(WebDriverType.REMOTE)
		    .setRemoteHubUrl("http://localhost:4444/wd/hub");
		
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setBrowserName("chrome");
		caps.setVersion("85");
		
		Config.setup()
		    .setUseProxy(true)
		    .setProxyHeaders(proxyHeaders)
		    .setDesiredCapabilities(caps);
		
		open("https://www.google.com");

		destroy();
	}
	
	@Test
	public void weCanSwitchToSomeFrameAndLocateAnElement() {
		open("https://www.google.com");
		switchFrame(0);
		WebElementWrapper el = f("h1");
		
		assertThat(el.getText()).isEqualTo("Voordat u doorgaat");
	}
}
