package be.ordina.madi.automationclass.plexex.tests;

import be.ordina.madi.automationclass.plexex.pages.app.BaseAppPage;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;

import java.io.IOException;

@Epic("POC Testing Plex")
@Feature("Play videos")
public class VideoPlayTest extends AppBaseTest {

    @Test
    @Story("Play video from details page with the play button.")
    @Description("A user can start the video playback from the details screen.\n" +
            "The user can click the dedicated play button, and the video starts playing.")
    public void aUserCanStartAVideoWithoutLoggingInFromTheDetailsPageWithThePlayButton()
            throws
            IOException,
            InterruptedException
    {

        String videoTitle = "Night of the Lepus";

        new BaseAppPage()
                .getAppHeaderBar()
                .searchFor(videoTitle)
                .verifyItemWithTitleIsPresent(videoTitle)
                .clickOnSearchResultWithTitle(videoTitle)
                .clickPlayButton()
                .verifyThatTheTimeAddsAsItPlays();
    }

    public void aUserCanPauseTheVideoAndSeeThePlayButton() {

    }

    public void aUserCanPauseTheVideoAndReturnToContinueWatching() {
        
    }

}
