package be.ordina.madi.automationclass.plexex.pages.app;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class DetailPage extends BaseAppPage {

    public WebElementWrapper getPrePlayPageHeaderElement() {
        return f("div[class*='PrePlaypageHeader-pageHeader']");
    }

    public WebElementWrapper getPrePlayContentScrollerElement() {
        return f("div[class*='PageContent-pageContentScroller']");
    }

    public WebElementWrapper getPrePlayDetailsContainer() {
        return getPrePlayContentScrollerElement().f("div[class*='PrePlayDetailsContainer-container']");
    }

    public WebElementWrapper getPrePlayPosterElement() {
        return getPrePlayDetailsContainer()
                .f("div[class*='PrePlayDetailsContainer-container'] " +
                        "div[class*='PosterCard-card'] " +
                        "div[class*='MetadataPosterCard-image']");
    }

    public WebElementWrapper getPrePlayActionBarElement() {
        return getPrePlayDetailsContainer().f("div[class*='PrePlayActionBar']");
    }

    public WebElementWrapper getPlayButtonElement() {
        return getPrePlayActionBarElement().f("button:nth-of-type(1)");
    }

    public WebElementWrapper getMarkAsPlayedButtonElement() {
        return getPrePlayActionBarElement().f("button:nth-of-type(2)");
    }

    public WebElementWrapper getMoreButtonElement() {
        return getPrePlayActionBarElement().f("button:nth-of-type(1)");
    }

    public WebElementWrapper getSpinnerButton() {
        return f("button[class*='SpinnerButton-button'][class*='Button-primary']");
    }

    //Steps
    @Step("Click the PLAY button.")
    public MoviePlayPage clickPlayButton() {
        getPlayButtonElement().click();

        if(getSpinnerButton().isDisplayed()) {
            getSpinnerButton().click();
        }

        return new MoviePlayPage();
    }
}
