package be.ordina.madi.automationclass.plexex.pages;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class CookieBannerPageModule {

    private WebElementWrapper getCookieBannerContainerElement() {
        return f("div.notification-bar.cookie-consent");
    }

    private WebElementWrapper getCookieMoreOptionsElement() {
        return getCookieBannerContainerElement().f("div.main-actions a.cookie-consent-settings");
    }

    private WebElementWrapper getCookieAcceptButtonElement() {
        return getCookieBannerContainerElement().f("div.main-actions a.cookie-consent-accept");
    }

    private WebElementWrapper getCookieTitleElement() {
        return getCookieBannerContainerElement().f("h4");
    }

}
