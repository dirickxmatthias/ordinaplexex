package be.ordina.madi.automationclass.plexex.pages.app;

import static org.assertj.core.api.Assertions.assertThat;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import be.ordina.madi.automationclass.plexex.utils.PlexWebAppUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;

import java.io.File;
import java.io.IOException;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class MoviePlayPage {

    public MoviePlayPage() {
        PlexWebAppUtils.waitForTheSpinnerstoBeGone();
    }

    public WebElementWrapper getTimeIndicationElement() {
        return f("button[class*='DurationRemaining']");
    }

    public WebElementWrapper getPlayPauseOverlayElement() {
        return f("button[class*='PlayButton-playButton']");
    }

    //steps
    @Step("Verify that the time moves on.")
    public MoviePlayPage verifyThatTheTimeAddsAsItPlays() throws InterruptedException, IOException {
        getPlayPauseOverlayElement().hover();
        Integer seconds_interval1 = getSecondsFromStringMovieDuration();
        File file = getTimeIndicationElement().getScreenshotAs(OutputType.FILE);
        Allure.addAttachment(
                "Video playback - interval 1",
                "image/png",
                FileUtils.openInputStream(file),
                ".png");
        //Must find something else here.
        Thread.sleep(3000);

        Integer seconds_interval2 = getSecondsFromStringMovieDuration();
        File file2 = getTimeIndicationElement().getScreenshotAs(OutputType.FILE);
        Allure.addAttachment(
                "Video playback - interval 1",
                "image/png",
                FileUtils.openInputStream(file2),
                ".png");

        assertThat(seconds_interval2).isGreaterThan(seconds_interval1);

        return this;
    }

    public Integer getSecondsFromStringMovieDuration() {
        return
            Integer
                .valueOf(
                    getTimeIndicationElement()
                        .getText()
                        .split("/")[0]
                        .split(":")[1]
                        .trim()
                );
    }

}
