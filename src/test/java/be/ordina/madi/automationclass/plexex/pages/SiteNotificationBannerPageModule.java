package be.ordina.madi.automationclass.plexex.pages;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class SiteNotificationBannerPageModule {

    private WebElementWrapper getTopNotificationContainerElement() {
        return f("div.notification-bar.site-notif");
    }
    private WebElementWrapper getTopNotificationIconElement() {
        return getTopNotificationContainerElement().f("div.icon-notifications_icon");
    }
    private WebElementWrapper getTopNotificationMessageElement() {
        return getTopNotificationContainerElement().f("div.notification-copy");
    }
    private WebElementWrapper getTopNotificationCloseButtonElement() {
        return getTopNotificationContainerElement().f("div.site-notif-close");
    }
}
