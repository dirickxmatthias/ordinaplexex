package be.ordina.madi.automationclass.plexex.tests;

import be.ordina.madi.automationclass.plexex.pages.app.BaseAppPage;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Test;

import java.io.IOException;

@Epic("POC Testing Plex")
@Feature("QuickSearch options")
public class SearchTest extends AppBaseTest {

    @Test
    @Story("Search from the top quick search bar.")
    @Description("Verify that the user can search for a movie through the quicksearch bar." +
            "The user can select a movie by clicking it, to land on the movie details page.")
    public void theUserCanSearchForAMovieInTheQuickSearchBar() throws InterruptedException, IOException {

        String titleToSearch = "Night of the Lepus";
        String searchTerm = "lepus";

        new BaseAppPage()
            .getAppHeaderBar()
            .searchFor(searchTerm)
            .verifyItemWithTitleIsPresent(titleToSearch)
            .clickOnSearchResultWithTitle(titleToSearch);
    }
}
