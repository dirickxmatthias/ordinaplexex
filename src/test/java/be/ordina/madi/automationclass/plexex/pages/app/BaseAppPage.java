package be.ordina.madi.automationclass.plexex.pages.app;

import be.mdi.selenium.simpleseframe.core.elements.WebElementCollection;
import be.ordina.madi.automationclass.plexex.utils.PlexWebAppUtils;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static be.mdi.selenium.simpleseframe.core.Statics.ff;
import static be.mdi.selenium.simpleseframe.core.Statics.driverWait;

public class BaseAppPage {

    public BaseAppPage() {
        PlexWebAppUtils.waitForTheSpinnerstoBeGone();
    }

    @Step("Go to the application's header bar.")
    public AppHeader getAppHeaderBar() {
        return new AppHeader();
    }

}
