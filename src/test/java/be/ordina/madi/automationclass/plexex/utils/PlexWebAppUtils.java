package be.ordina.madi.automationclass.plexex.utils;

import org.openqa.selenium.support.ui.ExpectedConditions;

import static be.mdi.selenium.simpleseframe.core.Statics.driverWait;
import static be.mdi.selenium.simpleseframe.core.Statics.ff;

public class PlexWebAppUtils {

    public static void waitForTheSpinnerstoBeGone() {
        driverWait().until(
                ExpectedConditions.invisibilityOfAllElements(
                        ff("div[class*='Spinner-spinner']").getWebElementList()
                )
        );
    }
}
