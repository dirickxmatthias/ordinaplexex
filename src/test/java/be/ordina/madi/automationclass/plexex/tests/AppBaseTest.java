package be.ordina.madi.automationclass.plexex.tests;

import be.mdi.selenium.simpleseframe.core.Config;
import be.mdi.selenium.simpleseframe.util.PropertyManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static be.mdi.selenium.simpleseframe.core.Statics.*;


public class AppBaseTest {
    
    @BeforeEach
    public void startSession() {
        Config.setup().load();
        open("https://app.plex.tv");
    }

    @AfterEach
    public void wrapSession() {
        //destroyBrowser();
    }

    @AfterAll
    public static void wrapTests() {
        //destroyAllBrowsers();
    }
}
