package be.ordina.madi.automationclass.plexex.pages.app;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import io.qameta.allure.Step;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class AppHeader extends BaseAppPage {

    // Identify elements
    public WebElementWrapper getHeaderNavBarElement() {
        return f("div.nav-bar");
    }

    public WebElementWrapper getLeftNavBarElement() {
        return getHeaderNavBarElement().f("div[class*='NavBar-left']");
    }

    public WebElementWrapper getMenuExpandElement() {
        return getLeftNavBarElement().f("button#id-0");
    }

    public WebElementWrapper getNavBarPlexLogoElement() {
        return getLeftNavBarElement().f("a[class*='NavBar-plexLogo']");
    }

    public WebElementWrapper getQuickSearchContainerElement() {
        return getLeftNavBarElement().f("div[class*='QuickSearchInput-container']");
    }

    public WebElementWrapper getQuickSearchInputElement() {
        return getQuickSearchContainerElement().f("input[class*='QuickSearchInput-searchInput']");
    }

    public WebElementWrapper getQuickSearchIconElement() {
        return getQuickSearchContainerElement().f("svg");
    }

    //Actions
    public AppHeader expandSideBar() {
        getMenuExpandElement().click();
        return this;
    }

    public BaseAppPage clickPlexLogo() {
        getNavBarPlexLogoElement().click();
        return new BaseAppPage();
    }

    public AppHeader inputSearchText(String inputText) {
        getQuickSearchInputElement().click();
        getQuickSearchInputElement().sendKeys(inputText);
        return this;
    }

    //Steps
    @Step("Search for the text '{inputText}' in the top Quicksearch bar.")
    public QuickSearchResult searchFor(String inputText) {
        inputSearchText(inputText);
        return new QuickSearchResult();
    }
}
