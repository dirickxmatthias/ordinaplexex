package be.ordina.madi.automationclass.plexex.pages;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class PreLoginHeaderPageModule {

    private WebElementWrapper getHomePageHeaderContainerElement() {
        return f("div.header");
    }

    private WebElementWrapper getHeaderPlexLogoElement() {
        return getHomePageHeaderContainerElement().f("div.logo a img");
    }

    private WebElementWrapper getHeaderMenuContainer() {
        return getHomePageHeaderContainerElement().f("div.menu-header");
    }

    private WebElementWrapper getHeaderMenuSubElement(String linkEnd) {
        return getHeaderMenuContainer().f(String.format("li a[href$='%s']", linkEnd));
    }

    private WebElementWrapper getHeaderMenuLiveTvElement() {
        return getHeaderMenuSubElement("watch-free-tv/");
    }

    private WebElementWrapper getHeaderMenuOnDemandElement() {
        return getHeaderMenuSubElement("watch-free/");
    }

    private WebElementWrapper getHeaderMenuYourMediaElement() {
        return getHeaderMenuSubElement("your-media/");
    }

    private WebElementWrapper getHeaderMenuCommunityElement() {
        return getHeaderMenuSubElement("blog/");
    }

    private WebElementWrapper getHeaderMenuPremiumElement() {
        return getHeaderMenuSubElement("plex-pass/");
    }

    private WebElementWrapper getHeaderSingInLinkElement() {
        return getHomePageHeaderContainerElement().f("a.signin");
    }

    private WebElementWrapper getHeaderSingUpButtonElement() {
        return getHomePageHeaderContainerElement().f("a.signup");
    }

    public PreLoginHeaderPageModule hooverOverLiveTv() {
        return this;
    }

}
