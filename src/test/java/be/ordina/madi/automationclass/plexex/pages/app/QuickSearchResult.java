package be.ordina.madi.automationclass.plexex.pages.app;

import java.io.File;
import java.io.IOException;

import io.qameta.allure.Allure;

import static org.assertj.core.api.Assertions.assertThat;

import be.mdi.selenium.simpleseframe.core.elements.WebElementCollection;
import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;

import static be.mdi.selenium.simpleseframe.core.Statics.f;

public class QuickSearchResult {

    //Identify elements
    public WebElementWrapper getQuickSearchResultContainerElement() {
        return f("div[class*='QuickSearchResults-container");
    }

    public WebElementCollection getSearchResultItemElements() {
        return getQuickSearchResultContainerElement().ff("div[class*='QuickSearchResult-button']");
    }

    public WebElementWrapper getSearchResultItemWithTitleElement(String theTitle) {
        return getQuickSearchResultContainerElement().f("div[class*='QuickSearchResult-button'] a[title='" + theTitle + "']");
    }

    //Actions
    public WebElementWrapper getResultWithTitle(String theTitle) {
        WebElementCollection results = getSearchResultItemElements();
        for(WebElementWrapper result : results) {
            if(theTitle.equalsIgnoreCase(
                    result.f("a").getAttribute("title"))
                ) {
                return result;
            }
        }
        return null;
    }

    @Step("Click the result with title '{theTitle}'")
    public DetailPage clickOnSearchResultWithTitle(String theTitle) {
        getResultWithTitle(theTitle).click();
        return new DetailPage();
    }

    @Step("Verify that the item with title '{titleToVerify}' is present in the results list.")
    public QuickSearchResult verifyItemWithTitleIsPresent(String titleToVerify) throws IOException {
        WebElementWrapper we = getSearchResultItemWithTitleElement(titleToVerify);

        File file = we.getScreenshotAs(OutputType.FILE);
        Allure.addAttachment("Result found", "image/png", FileUtils.openInputStream(file), ".png");

        String titleFound = we.getAttribute("title");

        assertThat(titleFound).isEqualTo(titleToVerify);

        return this;
    }

}
