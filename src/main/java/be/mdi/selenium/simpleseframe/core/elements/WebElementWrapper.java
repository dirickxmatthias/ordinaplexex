package be.mdi.selenium.simpleseframe.core.elements;

import java.util.List;

import be.mdi.selenium.simpleseframe.core.actions.UserActions;
import be.mdi.selenium.simpleseframe.core.locator.Locator;
import be.mdi.selenium.simpleseframe.core.locator.SearchContextWaitFactory;

import static be.mdi.selenium.simpleseframe.core.Driver.getDriver;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.FluentWait;

public class WebElementWrapper implements WebElement, Locatable {
	
	private WebElementWrapper parent;
	private WebElement we;
	private By by;
	private Integer listIndex;
	 
	public WebElementWrapper(WebElement webElement, By by) {
		initialize(webElement, by, null, null);
	}
	
	public WebElementWrapper(WebElement webElement, By by, WebElementWrapper parent) {
		initialize(webElement, by, parent, null);
	}
	
	public WebElementWrapper(WebElement webElement, By by, WebElementWrapper parent, Integer listIndex) {
		initialize(webElement, by, parent, listIndex);
	}
	
	private void initialize(WebElement webElement, By by, WebElementWrapper parent, Integer listIndex) {
		this.parent = parent;
		this.by = by;
		this.we = webElement;
		this.listIndex = listIndex;
	}
	
	public By getBy() {
		return this.by;
	}
	
	public WebElement getWebElement() {
		return this.we;
	}
	
	public WebElementWrapper f(By by) {
		return Locator.locateFirst(this, by);
	}
	
	public WebElementCollection ff(By by) {
		return Locator.locateAll(this, by);
	}
	
	public WebElementWrapper f(String css) {
		return f(By.cssSelector(css));
	}
	
	public WebElementCollection ff(String css) {
		return ff(By.cssSelector(css));
	}
	
	public WebElementWrapper fx(String xpath) {
		return f(By.xpath(xpath));
	}
	
	public WebElementCollection ffx(String xpath) {
		return ff(By.xpath(xpath));
	}
	
	public boolean hasParent() {
		return !(parent == null);
	}
	
	public boolean isListItem() {
		return !(listIndex == null);
	}

	public WebElementWrapper hover() {
		UserActions.hoverOver(this).execute();
		return this;
	}

	public WebElementWrapper moveTo(WebElementWrapper webElementWrapper) {
		UserActions.hoverOver(webElementWrapper).execute();
		return webElementWrapper;
	}
	
	public WebElementWrapper hitEnter() {
		we.sendKeys(Keys.ENTER);
		return this;
	}
	
	public WebElementWrapper hitReturn() {
		we.sendKeys(Keys.RETURN);
		return this;
	}
	
	public WebElementWrapper hitTab() {
		we.sendKeys(Keys.TAB);
		return this;
	}

	/* WebElement interface implementation */
	
	@Override
	public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
		return we.getScreenshotAs(target);
	}

	@Override
	public void click() {
		we.click();
	}

	@Override
	public void submit() {
		we.submit();
	}

	@Override
	public void sendKeys(CharSequence... keysToSend) {
		SearchContext sc = parent;
		if(parent == null) {
			sc = getDriver();
		}
		FluentWait<SearchContext> wait = SearchContextWaitFactory.getDefaultSearchContextWait(sc);
		wait.ignoring(ElementNotInteractableException.class);
		we = wait.until(c -> c.findElement(by));

		we.sendKeys(keysToSend);
	}

	@Override
	public void clear() {
		we.clear();
	}

	@Override
	public String getTagName() {
		return we.getTagName();
	}

	@Override
	public String getAttribute(String name) {
		return we.getAttribute(name);
	}

	@Override
	public boolean isSelected() {
		return we.isSelected();
	}

	@Override
	public boolean isEnabled() {
		return we.isEnabled();
	}

	@Override
	public String getText() {
		return we.getText();
	}

	@Override
	public List<WebElement> findElements(By by) {
		return we.findElements(by);
	}

	@Override
	public WebElement findElement(By by) {
		return we.findElement(by);
	}

	@Override
	public boolean isDisplayed() {
		return we.isDisplayed();
	}

	@Override
	public Point getLocation() {
		return we.getLocation();
	}

	@Override
	public Dimension getSize() {
		return we.getSize();
	}

	@Override
	public Rectangle getRect() {
		return we.getRect();
	}

	@Override
	public String getCssValue(String propertyName) {
		return we.getCssValue(propertyName);
	}

	@Override
	public Coordinates getCoordinates() {
		return ((RemoteWebElement)we).getCoordinates();
	}
}
