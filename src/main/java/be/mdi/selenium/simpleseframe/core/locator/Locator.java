package be.mdi.selenium.simpleseframe.core.locator;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import be.mdi.selenium.simpleseframe.core.elements.WebElementCollection;
import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;

/**
 * <p>Locator class to wrap the search logic and strategy in the framework.</p>
 * @author Matthias.Dirickx
 *
 */
public class Locator {
	
	public static WebElementWrapper locateFirst(SearchContext context, By by) {
		WebElement we = SearchContextWaitFactory
				.getSearchContextWait(context)
				.until(c -> c.findElement(by));
		
		return new WebElementWrapper(
				we,
				by,
				getContextIfWebElementWrapper(context)
				);
	}
	
	public static WebElementCollection locateAll(SearchContext context, By by) {
		List<WebElement> wel = SearchContextWaitFactory
				.getSearchContextWait(context)
				.until(c -> c.findElements(by));

		return new WebElementCollection(
				wel,
				by,
				getContextIfWebElementWrapper(context)
				);
	}
	
	private static WebElementWrapper getContextIfWebElementWrapper(SearchContext context) {
		
		WebElementWrapper parent = null;
		
		if(context instanceof WebElementWrapper) {
			parent = (WebElementWrapper)context;
		}
		
		return parent;
	}

}
