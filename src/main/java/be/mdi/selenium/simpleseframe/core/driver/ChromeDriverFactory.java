package be.mdi.selenium.simpleseframe.core.driver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import be.mdi.selenium.simpleseframe.core.Config;

/**
 * <p>Factory class to produce the chrome driver objects.</p>
 * <p>Factory classes like this are called by the {@link DriverFactory}.</p>
 * <p>The factory class should contain all functions to instantiate a {@link WebDriver} of type {@link ChromeDriver}.</p>
 * 
 * @author Matthias.Dirickx
 *
 */
public class ChromeDriverFactory {
	
	private ChromeOptions chromeOptions;
	
	public ChromeDriverFactory() {
		this.chromeOptions = Config.getChromeOptions();
	}
	
	public ChromeDriver buildAndGetDriver() {
		return new ChromeDriver(chromeOptions);
	}

}
