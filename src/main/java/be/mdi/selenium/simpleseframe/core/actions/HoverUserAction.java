package be.mdi.selenium.simpleseframe.core.actions;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class HoverUserAction implements UserAction {

    Actions action;

    public HoverUserAction(WebDriver driver, WebElementWrapper webElementWrapper) {
        this.action = new Actions(driver).moveToElement(webElementWrapper);
    }

    public void execute() {
        action.build().perform();
    }

}
