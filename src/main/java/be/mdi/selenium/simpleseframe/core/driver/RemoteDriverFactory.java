package be.mdi.selenium.simpleseframe.core.driver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import be.mdi.selenium.simpleseframe.core.Config;

/**
 * <p>Factory class to produce the remote driver objects.</p>
 * <p>Factory classes like this are called by the {@link DriverFactory}.</p>
 * <p>The factory class should contain all functions to instantiate a {@link WebDriver} of type {@link RemoteWebDriver}.</p>
 * 
 * <p>This factory requires extra setup from the {@link Config} class.</p>
 * 
 * <ul>
 * <li>remoteHubUrl</li>
 * </ul>
 * @author Matthias.Dirickx
 *
 */
public class RemoteDriverFactory {
	
	private DesiredCapabilities desiredCapabilities;
	private URL remoteUrl;
	
	public RemoteDriverFactory() {
		this.desiredCapabilities = Config.getDesiredCapabilities();
		try {
			this.remoteUrl = new URL(Config.getRemoteHubUrl());
		} catch (MalformedURLException e) {
			//Do nothing - shouldn't happen and otherwise RemoteWebDriver will complain for null input.
		}
	}
	
    public RemoteWebDriver buildAndGetDriver() {		
		return new RemoteWebDriver(remoteUrl, desiredCapabilities);
	}

}
