package be.mdi.selenium.simpleseframe.core.driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import be.mdi.selenium.simpleseframe.core.Config;

/**
 * <p>The {@link DriverFactory} class serves the drivers for the framework.</p>
 * <p>The class is concerned with the instantiation, but not with the management.</p>
 * <p>For the management of the used drivers, please refer to:
 * <ul>
 * <li>{@link ThreadedWebDriver} for serving the correct driver to the correct thread</li>
 * <li>{@link Driver} for implemented actions concerning the WebDriver</li>
 * </ul>
 * @author Matthias.Dirickx
 *
 */
public class DriverFactory {
	
	private WebDriverType driverType;
	private String driverBinaryUrl;
	
	private DriverFactory() {
		driverType = Config.getDriverType();
		driverBinaryUrl = Config.getDriverBinary();
		setBinaryPath();
	}
	
	private void setBinaryPath() {
		String propertyName = "webdriver.chrome.driver";
		
		switch(driverType) {
			case CHROME:
				propertyName = "webdriver.chrome.driver";
				break;
			case HTML_UNIT:
				break;
			default:
				propertyName = "webdriver.chrome.driver";
		}
		
		System.setProperty(propertyName, driverBinaryUrl);
	}
	
	private void applyDriverOptionsFromProperties(WebDriver driver) {
		//Start maximized
	    if(Config.isStartMaximized()) {
	    	if(!Config.isHeadlessMode()) {
		    	driver.manage().window().maximize();
	    	}
	    }
	    
	    //Implicit wait
	    driver.manage()
	        .timeouts()
	        .implicitlyWait(
	        		Config.getDriverImplicitWaitTimeout(),
	        		TimeUnit.MILLISECONDS);
	}
	
	private WebDriver buildAndGetRequestedDriver() {
		
		WebDriver driver;
		
		switch(driverType) {
			case CHROME:
				driver = new ChromeDriverFactory().buildAndGetDriver();
				break;
			case FIREFOX:
				driver = new FirefoxDriverFactory().buildAndGetDriver();
				break;
			case REMOTE:
				driver = new RemoteDriverFactory().buildAndGetDriver();
				break;
			case HTML_UNIT:
				driver = new HtmlUnitDriverFactory().buildAndGetDriver();
				break;
			default:
				driver = new ChromeDriverFactory().buildAndGetDriver();
				break;
		}
		
		applyDriverOptionsFromProperties(driver);
		
		return driver;
	}
	
	public static WebDriver getDriver() {
		return new DriverFactory().buildAndGetRequestedDriver();
	}
}
