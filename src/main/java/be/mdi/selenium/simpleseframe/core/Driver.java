package be.mdi.selenium.simpleseframe.core;


import be.mdi.selenium.simpleseframe.core.driver.ThreadedWebDriver;
import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.SessionStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.html5.RemoteWebStorage;

import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * <p>Set of static methods to access the WebDriver object.</p>
 * @author Matthias.Dirickx
 *
 */
public class Driver {
	
	/**
	 * <p>Returns the WebDriver object for the current thread.</p>
	 * 
	 * @return WebDriver
	 */
	public static WebDriver getDriver() {
		return ThreadedWebDriver.getDriver();
	}
	
	public static void destroy() {
		ThreadedWebDriver.destroyDriver();
	}
	
	public static void clean() {
		deleteAllCookies();
		getLocalStorage().clear();
		getSessionStorage().clear();
	}
	
	public static Set<Cookie> deleteAllCookies() {
		Set<Cookie> cookies = getDriver().manage().getCookies();
		getDriver().manage().deleteAllCookies();
		return cookies;
	}
	
	public static WebStorage getWebStorage() {
		WebDriver driver = getDriver();
		switch(Config.getDriverType()) {
		    case REMOTE:
		    	RemoteExecuteMethod executeMethod = new RemoteExecuteMethod((RemoteWebDriver) driver);
		    	return new RemoteWebStorage(executeMethod);
		    default:
			    return (WebStorage)driver;
		}
	}
	
	public static LocalStorage getLocalStorage() {
		return getWebStorage().getLocalStorage();
	}
	
	public static SessionStorage getSessionStorage() {
		return getWebStorage().getSessionStorage();
	}
	
	public static void destroyAll() {
		ThreadedWebDriver.destroyAllDrivers();
	}
	
	public static void goTo(String url) {
		getDriver().get(url);
	}
	
	public static void switchFrame(WebElement webElement) {
		getDriver().switchTo().frame(webElement);
	}
	
	public static void switchFrame(int frameIndex) {
		getDriver().switchTo().frame(frameIndex);
	}
	
	public static void switchFrame(String nameOrId) {
		getDriver().switchTo().frame(nameOrId);
	}
	
	public static void switchToDefaultFrame() {
		getDriver().switchTo().defaultContent();
	}
	
	public static WebElementWrapper switchToFocusedElement() {
		return new WebElementWrapper(getDriver().switchTo().activeElement(), null);
	}
}
