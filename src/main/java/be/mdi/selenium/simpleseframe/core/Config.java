package be.mdi.selenium.simpleseframe.core;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;

import be.mdi.selenium.simpleseframe.core.driver.WebDriverType;
import be.mdi.selenium.simpleseframe.core.proxy.SeleniumProxy;
import be.mdi.selenium.simpleseframe.util.PropertyManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * <p>
 * This class provides the options for the framework.
 * All options should be set through this class.
 * Any custom properties may be set through the PropertyManger class which is also used by this Config class to load user-provided defaults.
 * </p>
 * <p>
 * The user provided defaults must be in the format of a standard properties file.
 * These items are <i>not</i> loaded on instantiation. To load the items you must call {@link #load()} to have the properties loaded.
 * </p>
 * <p>Example of configuration:</br>
 * <i>For a default file "selelium.properties"</i>
 * <pre>{@code
 * Config.setup().load();
 * }</pre>
 * <p>The class is statically called, and then {@link #setup} is called to access the shared instance. After this, we chain {@link #load()}
 * to load our default configuration in the "selenium.properties" file. To use a different properties name, use {@link #load(String)}.</p>
 * </p>
 * <table border="1" style="border-collapse: collapse;">
 * <caption>Options</caption>
 * <tr><th>Property name</th><th>Variable in Config</th><th>Default</th></tr>
 * <tr><td>driver</td><td>{@link #driverType}</td><td>chrome</td></tr>
 * <tr><td>driver.binary</td><td>{@link #driverBinary}</td><td>null</td></tr>
 * <tr><td>driver.wait.timeout</td><td>{@link #driverFluentWaitTimeout}</td><td>10 (seconds)</td></tr>
 * <tr><td>driver.wait.poll</td><td>{@link #driverFluentWaitPollingInterval}</td><td>200 (milliseconds)</td></tr>
 * <tr><td>driver.start_maximized</td><td>{@link #startMaximized}</td><td>true</td></tr>
 * <tr><td>driver.headless</td><td>{@link #headlessMode}</td><td>false</td></tr>
 * <tr><td>driver.quit.after.test</td><td>{@link #quitDriverAfterTest}</td><td>true</td></tr>
 * <tr><td>driver.clean.after.test</td><td>{@link #cleanDriverAfterTest}</td><td>true</td></tr>
 * <tr><td>browser.name</td><td>{@link #browserName}</td><td>chrome</td></tr>
 * <tr><td>browser.version</td><td>{@link #browserVersion}</td><td>85</td></tr>
 * <tr><td>remote.hub.url</td><td>{@link #remoteHubUrl}</td><td>http://127.0.0.1:4444/wd/hub</td></tr>
 * </table>
 * 
 * <p>To get a local copy of the file, you can use the {@link #copyLocalExample(String)} method.</p>
 * @author Matthias.Dirickx
 * 
 */
public class Config {
	
	private WebDriverType driverType;
	private Duration driverFluentWaitTimeout;
	private Duration driverFluentWaitPollingInterval;
	private Long driverImplicitWaitTimeoutMilliseconds;
	
	private boolean acceptInsecureSsl;
	
	private boolean useProxy;
	private HashMap<String, String> proxyHeaders;
	
	private Proxy proxy;
	
	private ChromeOptions chromeOptions;
	private FirefoxOptions firefoxOptions;
	private DesiredCapabilities desiredCapabilities;
	
	private DesiredCapabilities generalCapabilities;
	
	private boolean headlessMode;
	
	private boolean cleanDriverAfterTest;
	private boolean quitDriverAfterTest;
	
	private boolean startMaximized;
	
	private String remoteHubUrl;
	private String browserName;
	private String browserVersion;
	
	private String driverBinary;
	
	private Config() {
		initialize();
	}
	
	/**
	 * <p>Initialize the class variables with defaults. The defaults are hardcoded into this class.</p>
	 * <p>These defaults are overwritten with the properties defined in
	 * selenium.properties if you choose to load the file.</p>
	 */
	private Config initialize() {
		
		// Properties file name
		PropertyManager.propertyFile = "selenium.properties";
		
		//Driver & Browser
        driverType = WebDriverType.CHROME;
        driverFluentWaitTimeout = Duration.ofSeconds(5);
        driverFluentWaitPollingInterval = Duration.ofMillis(200);
        driverImplicitWaitTimeoutMilliseconds = 0L;
		browserName = "chrome";
		browserVersion = "86";
		remoteHubUrl = "http://127.0.0.1:4444/wd/hub";
		
		//Driver Settings
		acceptInsecureSsl = true;
		
		headlessMode = false;
		startMaximized = true;
		
		useProxy = false;
		proxyHeaders = new HashMap<>();
		
		chromeOptions = new ChromeOptions();
		firefoxOptions = new FirefoxOptions();
		desiredCapabilities = new DesiredCapabilities();
		generalCapabilities = new DesiredCapabilities();
		
		//Test setup support variables
		cleanDriverAfterTest = true;
		quitDriverAfterTest = true;
		return this;
	}
	
	/**
	 * Holder for the Config singleton.
	 * @author Matthias.Dirickx
	 *
	 */
	private static class ConfigHolder {
		private static Config instance = new Config();
	}
	
	/**
	 * <p>This method provides access to the Config singleton object.</p>
	 * <p>This way the Config object should be usable fluently without wrapping it again to have it accessible over classes.</p>
	 * <pre>{@code
	 * Config.setup()
	 *     .load("someother.properties)
	 *     .setUseProxy(false);
	 *     }</pre>
	 * @return Config
	 */
	public static Config setup() {
		return getInstance();
	}
	
	/**
	 * <p>Load a config file.</p>
	 * <p>To set the properties file / config file, use either:
	 * <ul>
	 * <li>set filename with {@link #setPropertiesFileName(String)} and then call {@link #load()}</li>
	 * <li>call {@link #load(String)} directly. This method loads the file given as the argument.</li>
	 * </ul>
	 * Note that if you call it from a Selenium test, then you should put it under src/test/resources
	 * and not under src/main/resources because of the scope of the class loader.</p>
	 * @return {@link Config}
	 */
	public Config load() {
		setupProvidedProperties();
		return this;
	}
	
	/**
	 * <p>Load a config file.</p>
	 * <p>Note that if you call it from a Selenium test, then you should put it under src/test/resources
	 * and not under src/main/resources because of the scope of the class loader.</p>
	 * @param propertiesFileName - Resource file to load starting from ClassLoader.
	 * @return {@link Config}
	 */
	public Config load(String propertiesFileName) {
		PropertyManager.loadFile(propertiesFileName);
		return load();
	}
	
	/**
	 * <p>Reset the configuration object to it's initial state.</p>
	 * <p>The initial state are the hardcoded default values, enriched with the
	 * items of the properties file registered in the {@link PropertyManager#propertyFile} variable.</p>
	 */
	public static void reset() {
		ConfigHolder.instance = new Config().load();
	}
	
	/**
	 * <p>Return the properties file name.
	 * The properties file name is loaded as a resource from the class loader.</p>
	 * @return String
	 */
	public static String getPropertiesFileName() {
		return PropertyManager.propertyFile;
	}

	/**
	 * <p>Set the properties file name.</p>
	 * <p>The properties file name is loaded as a resource through the class loader.</p>
	 * <p>The file name used must hence be under the resources folder of the class loader
	 * used by the application that is running the Config class.</p>
	 * <p>We are using a brittle mechanism where certain parameters are translated to variables.
	 * This translation is hardcoded into the {@link #initialize()} method of this class.</p>
	 * <p>If you want to use a different properties file, then this should be the first action.</p>
	 * <p>Note that if you want to load the properties from a file,
	 * then you must call the {@link #load()} method or the {@link #load(String)} method.</p>
	 * <p>The {@link #load()} method will load the default properties file named 'selenium.properties'.
	 * This file is loaded as a resource with the class loader. This means it should be  in the
	 * resources folder of the app with which you call it.</p>
	 * <p>You should note that this method will also load the variables from the file into memory.
	 * Any exceptions are caught, so you'll run against nullpointerexceptions if there's things that are not there.</p>
	 * <p>Note that if you call it from a Selenium test, then you should put it under src/test/resources
	 * and not under src/main/resources because of the scope of the class loader.</p>
	 * @param propertiesFileName - Resource file to load starting from ClassLoader
	 * @return Config
	 */
	public Config setPropertiesFileName(String propertiesFileName) {
		load(propertiesFileName);
		return this;
	}
	
	/**
	 * <p>Get the WebDriverType.</p>
	 * @return WebDriverType
	 */
	public static WebDriverType getDriverType() {
		return getInstance().driverType;
	}
	
	/**
	 * <p>Set the WebDriverType.</p>
	 * <p><b>Note:</b><br></br>
	 * The WebDriverType is not the same as the browser name. To define a browser name to use, use {@link #setBrowserName(String)}. </p>
	 * <p>The WebDriverType sets the driver to use. This may be chromedriver, geckodriver, remote webdriver or others.</p>
	 * 
	 * <p><i>This path can be set with the <b>driver.headless</b> property so you can load it with the {@link #load()} method.</i></p>
	 * <p>The options are:
	 * <ul>
	 * <li>chrome</li>
	 * <li>firefox</li>
	 * <li>remote</li>
	 * </ul>
	 * <p>The values are marshaled into a WebDriverType object by a {@link WebDriverType#valueOf(String)} 
	 * call where the String is transformed to all capital letters.</p>
	 * @param driverType WebDriverType
	 * @return Config
	 */
	public Config setDriverType(WebDriverType driverType) {
		this.driverType = driverType;
		return this;
	}
	
	/**
	 * <p>Get the duration of the timeout for the webdriver wait.</p>
	 * <p>This is the standard wait that is embedded into the framework.
	 * This can be bypassed by setting the timeout to zero seconds.</p>
	 * @return Duration
	 */
	public static Duration getDriverFluentWaitTimeout() {
		return getInstance().driverFluentWaitTimeout;
	}

	/**
	 * <p>Set the Fluent wait timeout value used in the location strategy.
	 * @param driverFluentWaitTimeout Duration
	 * @return Config
	 */
	public Config setDriverFluentWaitTimeout(Duration driverFluentWaitTimeout) {
		this.driverFluentWaitTimeout = driverFluentWaitTimeout;
		return this;
	}

	/**
	 * <p>Get the duration of the polling interval for the webdriver wait.</p>
	 * <p>This is the standard wait that is embedded into the framework.
	 * This can be bypassed by setting the timeout to zero seconds.</p>
	 * @return Duration
	 */
	public static Duration getDriverFluentWaitPolling() { return getInstance().driverFluentWaitPollingInterval; }

	/**
	 * <p>Set the Fluent wait polling value used in the location strategy.
	 * @param driverFluentWaitTimeout Duration
	 * @return Config
	 */
	public Config setDriverFluentWaitPolling(Duration driverFluentWaitPollingInterval) {
		this.driverFluentWaitPollingInterval = driverFluentWaitPollingInterval;
		return this;
	}

	/**
	 * <p>Return the timeout for the implicit wait.</p>
	 * <p>The timeunit is not configurable in the framework, and is set milliseconds.</p>
	 * <p>For custom implementation you can always either set your own driver or
	 * manipulate the WebDriver object directly.</p>
	 * <p><i>This path can be set with the <b>driver.wait.implicit.timeout</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return Long
	 */
	public static Long getDriverImplicitWaitTimeout() {
		return getInstance().driverImplicitWaitTimeoutMilliseconds;
	}
	
	/**
	 * <p>Set the timeout for the implicit wait.</p>
	 * <p>The timeunit is not configurable in the framework, and is set milliseconds.</p>
	 * <p>For custom implementation you can always either set your own driver or
	 * manipulate the WebDriver object directly.</p>
	 * <p><i>This path can be set with the <b>driver.wait.implicit.timeout</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param driverImplicitWaitTimeoutMilliseconds Long
	 * @return Config
	 */
	public Config setDriverImplicitWaitTimeout(Long driverImplicitWaitTimeoutMilliseconds) {
		this.driverImplicitWaitTimeoutMilliseconds = driverImplicitWaitTimeoutMilliseconds;
		return this;
	}

	/**
	 * <p>Return the boolean saying whether to use a proxy or not.</p>
	 * <p>If true, then a proxy is created (BrowserMob) when creating the driver.</p>
	 * @return boolean
	 */
	public static boolean getUseProxy() {
		return getInstance().useProxy;
	}
	
	/**
	 * <p>Set whether to use a proxy.</p>
	 * <p>If true, then a proxy is created (BrowserMob) when creating the driver.
	 * This is for local testing only. For more robust distributed testing you
	 * should run your own server and set the proxy object by this configuration
	 * instead of relying on this framework to generate a generic proxy.</p>
	 * @param useProxy boolean
	 * @return Config
	 */
	public Config setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
		return this;
	}
	
	/**
	 * <p>Get headers to be used for the local proxy.</p>
	 * @return {@code HashMap<String, String>}
	 */
	public static HashMap<String, String> getProxyHeaders() {
		return getInstance().proxyHeaders;
	}
	
	/**
	 * <p>Set the headers to be used for the local proxy.</p>
	 * <p>Note that these headers will not automatically be added to proxies used
	 * that are not managed by this framework. It is not advised to use the proxy
	 * from this framework for distributed testing.</p>
	 * <p>Changing headers on the fly is not supported. To do that, you have to
	 * get the proxy object by calling {@link SeleniumProxy#getBrowserMobProxy()} in
	 * order to modify and restart it.</p>
	 * @param proxyHeaders {@code Map<String, String>}
	 * @return Config
	 */
	public Config setProxyHeaders(HashMap<String, String> proxyHeaders) {
		this.proxyHeaders = proxyHeaders;
		return this;
	}
	
	/**
	 * <p>Return the selenium Proxy object used for the capabilities.</p>
	 * @return Proxy
	 */
	public static Proxy getProxy() {
		return getInstance().proxy;
	}
	
	/**
	 * <p>Set the selenium proxy to be used in the capabilities.</p>
	 * 
	 * @param proxy Proxy (selenium Proxy)
	 * @return Config
	 */
	public Config setProxy(Proxy proxy) {
		this.proxy = proxy;
		return this;
	}
	
	/**
	 * <p>Get the {@link DesiredCapabilities} stored in the Config class.</p>
	 * 
	 * @return DesiredCapabilities
	 */
	public static DesiredCapabilities getDesiredCapabilities() {
		getInstance().setupGeneralCapabilities();
		return getInstance().desiredCapabilities.merge(getInstance().generalCapabilities);
	}

	/**
	 * <p>Set the {@link DesiredCapabilities} object used for the remote driver.</p>
	 * @param desiredCapabilities DesiredCapabilities
	 * @return Config
	 */
	public Config setDesiredCapabilities(DesiredCapabilities desiredCapabilities) {
		setupGeneralCapabilities();
		this.desiredCapabilities = desiredCapabilities.merge(generalCapabilities);
		return this;
	}
	
	/**
	 * <p>Get the generalCapabilities.</p>
	 * @return DesiredCapabilities
	 */
	public static DesiredCapabilities getGeneralCapabilities() {
		getInstance().setupGeneralCapabilities();
		return getInstance().generalCapabilities.merge(getInstance().generalCapabilities);
	}
	
	/**
	 * <p>Set the generalCapabilities.</p>
	 * <p>These generic capabilities are managed by the user and are merged with all the specialized Options like
	 * {@link #chromeOptions} and {@link #firefoxOptions}.</p>
	 * <p>In extremis you could omit loading a specialized object and put everything into a {@link DesiredCapabilities}
	 * object loaded by calling {@link #setGeneralCapabilities(DesiredCapabilities)}.</p>
	 * @param generalCapabilities DesiredCapabilities
	 * @return Config
	 */
	public Config setGeneralCapabilities(DesiredCapabilities generalCapabilities) {
		this.generalCapabilities = generalCapabilities;
		return this;
	}

	/**
	 * <p>Get he ChromeOptions object stored in the Config.</p>
	 * <p>The ChromeOptions object is merged with the GeneralCapabilities
	 * object in the generalCapabilities variable holding the generic options
	 * for the driver, that are valid for all WebDrivers created.</p>
	 * @return ChromeOptions
	 */
	public static ChromeOptions getChromeOptions() {
		getInstance().setupGeneralCapabilities();
		getInstance().chromeOptions.setHeadless(Config.isHeadlessMode());
		return getInstance().chromeOptions.merge(getInstance().generalCapabilities);
	}
	
	/**
	 * <p><b>Note:<br></br>
	 * The ChromeOptions are merged with {@link #generalCapabilities}.</b><br></br>
	 * This to be able to generically load overall options for all drivers.</p>
	 * <p>If you do not set up this generalCapabilities, then the empty object will be merged resulting in no changes.</p>
	 * 
	 * @param chromeOptions ChromeOptions
	 * @return Config
	 */
	public Config setChromeOptions(ChromeOptions chromeOptions) {
		setupGeneralCapabilities();
		this.chromeOptions = chromeOptions.merge(generalCapabilities);
		return this;
	}
	
	/**
	 * <p>Get he FirefoxOptions object stored in the Config.</p>
	 * <p>The FirefoxOptions object is merged with the GeneralCapabilities
	 * object in the generalCapabilities variable holding the generic options
	 * for the driver, that are valid for all WebDrivers created.</p>
	 * @return FirefoxOptions
	 */
	public static FirefoxOptions getFirefoxOptions() {
		getInstance().setupGeneralCapabilities();
		getInstance().firefoxOptions.setHeadless(Config.isHeadlessMode());
		return getInstance().firefoxOptions.merge(getInstance().generalCapabilities);
	}
	
	/**
	 * <p><b>Note:<br></br>
	 * The FirefoxOptions are merged with {@link #generalCapabilities}.</b><br></br>
	 * This to be able to generically load overall options for all drivers.</p>
	 * <p>If you do not set up this generalCapabilities, then the empty object will be merged resulting in no changes.</p>
	 * 
	 * @param firefoxOptions FirefoxOptions
	 * @return Config
	 */
	public Config setFirefoxOptions(FirefoxOptions firefoxOptions) {
		setupGeneralCapabilities();
		this.firefoxOptions = firefoxOptions.merge(generalCapabilities);
		return this;
	}
	
	/**
	 * <p>Get headless mode boolean.</p>
	 * <p>When true, start the driver in headless mode.</p>
	 * <p><i>This path can be set with the <b>driver.headless</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return boolean
	 */
	public static boolean isHeadlessMode() {
		return getInstance().headlessMode;
	}
	
	/**
	 * <p>Set whether to run the browser in headless mode.</p>
	 * <p>Note that this option is not available for all browsers.</p>
	 * 
	 * <p><i>This path can be set with the <b>driver.headless</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param headlessMode boolean
	 * @return Config
	 */
	public Config setHeadlessMode(boolean headlessMode) {
		this.headlessMode = headlessMode;
		return this;
	}
	
	/**
	 * <p>Get boolean with request to clean up after each test or not.</p>
	 * <p>The default value is true.</p>
	 * <p><i>This path can be set with the <b>driver.clean.after.test</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return boolean
	 */
	public static boolean isCleanDriverAfterTest() {
		return getInstance().cleanDriverAfterTest;
	}

	/**
	 * <p>Set whether to clean the driver after eacht test.</p>
	 * <p>There currently are not any listeners defined, so it is only
	 * a convenience item to store the property you'll use in @Before or @After statements.<p>
	 * 
	 * <p><i>This path can be set with the <b>driver.clean.after.test</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param cleanDriverAfterTest boolean
	 * @return Config
	 */
	public Config setCleanDriverAfterTest(boolean cleanDriverAfterTest) {
		this.cleanDriverAfterTest = cleanDriverAfterTest;
		return this;
	}

	/**
	 * <p>Get boolean with request to quit the driver after each test or not.</p>
	 * <p>The default value is true.</p>
	 * <p><i>This path can be set with the <b>driver.quit.after.test</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return boolean
	 */
	public static boolean isQuitDriverAfterTest() {
		return getInstance().quitDriverAfterTest;
	}

	/**
	 * <p>Set whether to quit the driver after each test.</p>
	 * <p>Quitting the driver after each test is more expensive. On the other hand
	 * you have a guarantee that the driver you start with will be clean.</p>
	 * <p>If for one reason or another you have dependencies in tests and you run
	 * them one after the other on the same browser instance you may leave it open
	 * for example. Although this is not advised.</p>
	 * <p><i>This path can be set with the <b>driver.quit.after.test</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param quitDriverAfterTest boolean
	 * @return Config
	 */
	public Config setQuitDriverAfterTest(boolean quitDriverAfterTest) {
		this.quitDriverAfterTest = quitDriverAfterTest;
		return this;
	}

	/**
	 * <p>Get the boolean stating whether it is desired to start maximized.</p>
	 * <p><i>This path can be set with the <b>driver.start_maximized</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return
	 */
	public static boolean isStartMaximized() {
		return getInstance().startMaximized;
	}

	/**
	 * <p>Set if the WebDriver should be maximized.</p>
	 * <p>Note that if you're running headless you need to set height and width.</p>
	 * <p><i>This path can be set with the <b>driver.start_maximized</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param startMaximized boolean
	 * @return Config
	 */
	public Config setStartMaximized(boolean startMaximized) {
		this.startMaximized = startMaximized;
		return this;
	}

	/**
	 * <p>Get the current remote hub URL value.</p>
	 * <p><i>This path can be set with the <b>remote.hub.url</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return
	 */
	public static String getRemoteHubUrl() {
		return getInstance().remoteHubUrl;
	}

	/**
	 * <p>Set the remote hub URL.</p>
	 * <p>The standard default value is http://127.0.0.1:4444/wd/hub.</p>
	 * <p>This variable is used in the RemoteWebDriver object. To call this driver,
	 * you need to put {@link #driverType} to {@link WebDriverType#REMOTE}.
	 * You may set this option with {@link #setDriverType(WebDriverType)}.
	 * <p><i>Note that this does not at all instantiate a grid! It is merely the address to contact starting from a Remote WebDriver.</i></p>
	 * <p><i>This path can be set with the <b>remote.hub.url</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param remoteHubUrl String
	 * @return Config
	 */
	public Config setRemoteHubUrl(String remoteHubUrl) {
		this.remoteHubUrl = remoteHubUrl;
		return this;
	}

	/**
	 * <p>Get the browser name.</p>
	 * <p><i>This path can be set with the <b>browser.name</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return String
	 */
	public static String getBrowserName() {
		return getInstance().browserName;
	}

	/**
	 * <p>Set the name of the browser.</p>
	 * <p>The borwser name is useful with RemoteWebDriver. For the rest it is purely informative.</p>
	 * <p>Note that the {{@link #browserName} and {{@link #driverType} items are fundamentally different here.</p>
	 * <p>The browserName item is used in the RemoteWebDriver setup. The driverType is for the choice of driver.</p>
	 * <p>For a local chrome you need to put:
	 * <table>
	 * <caption>Browser name / Driver type</caption>
	 * <tr><th>browserName</th><th>driverType</th></tr>
	 * <tr><td>chrome</td><td>chrome</td></tr>
	 * </table>
	 * When you need to launch a chrome instance remotely, you put:
	 * <table>
	 * <tr><th>browserName</th><th>driverType</th></tr>
	 * <tr><td>chrome</td><td>remote</td></tr>
	 * </table>
	 * <p><i>This path can be set with the <b>browser.name</b> property so you can load it with the {@link #load()} method.</i></p>
	 * 
	 * @param browserName String
	 * @return Config
	 */
	public Config setBrowserName(String browserName) {
		this.browserName = browserName;
		return this;
	}

	/**
	 * <p> Get the browser version.</p>
	 * <p><i>This path can be set with the <b>browser.version</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return String
	 */
	public static String getBrowserVersion() {
		return getInstance().browserVersion;
	}

	/**
	 * <p>Set the browser version to be used.</p>
	 * <p>This is for identification purposes only. You may omit this if you point straight to the binary to be used.</p>
	 * <p>The version is stored as a String. Make sure to much your configuration.</p>
	 * <p><i>This path can be set with the <b>browser.version</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param browserVersion
	 * @return Config
	 */
	public Config setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
		return this;
	}

	/**
	 * <p>Get the driver binary path.</p>
	 * <p><i>This path can be set with the <b>driver.binary</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @return String
	 */
	public static String getDriverBinary() {
		return getInstance().driverBinary;
	}

	/**
	 * <p>Set the driver binary path.</p>
	 * <p>The path should be a full path, and not a relative one.</p>
	 * <p><i>This path can be set with the <b>driver.binary</b> property so you can load it with the {@link #load()} method.</i></p>
	 * @param driverBinary String - path to the driver binary.
	 * @return Config
	 */
	public Config setDriverBinary(String driverBinary) {
		this.driverBinary = driverBinary;
		return this;
	}

	/**
	 * Construct general capabilities to be used with the variables encoded in the configuration.
	 */
	private void setupGeneralCapabilities() {
		if(useProxy) {
			if(proxy == null) {
				proxy = SeleniumProxy.getProxyForCapabilities();
			}
			generalCapabilities.setCapability(
					CapabilityType.PROXY,
					proxy
					);
		}
		
		generalCapabilities.setCapability(
				CapabilityType.ACCEPT_INSECURE_CERTS, acceptInsecureSsl
				);
	}
	
	/**
	 * This is a mapping of the properties in the file to the properties in the config object.
	 * In essence the Config object is used throughout the framework. The PropertyManager object
	 * is used to the properties file, but is not called as such.
	 */
	private void setupProvidedProperties() {
		
		driverType = WebDriverType.valueOf(
				PropertyManager.getProperty(
						"driver",
						driverType.toString()).toUpperCase()
				);
		driverFluentWaitTimeout = Duration.ofSeconds(
				PropertyManager.getLongProperty(
						"driver.wait.timeout",
						driverFluentWaitTimeout.getSeconds()
						)
				);
		driverFluentWaitPollingInterval = Duration.ofMillis(
				PropertyManager.getLongProperty(
						"driver.wait.poll",
						driverFluentWaitPollingInterval.toMillis()
						)
				);
		driverImplicitWaitTimeoutMilliseconds = PropertyManager.getLongProperty(
				"driver.wait.implicit.timeout",
				driverImplicitWaitTimeoutMilliseconds
				);
		browserName = PropertyManager.getProperty(
				"browser.name",
				browserName
				);
		startMaximized = PropertyManager.getBooleanProperty(
				"driver.start_maximized",
				startMaximized
				);
		headlessMode = PropertyManager.getBooleanProperty(
				"driver.headless",
				headlessMode);
		driverBinary = PropertyManager.getProperty(
				"driver.binary",
				driverBinary
				);
		remoteHubUrl = PropertyManager.getProperty(
				"remote.hub.url",
				remoteHubUrl
				);
		cleanDriverAfterTest = PropertyManager.getBooleanProperty(
				"driver.clean.after.test",
				cleanDriverAfterTest);
		quitDriverAfterTest = PropertyManager.getBooleanProperty(
				"driver.quit.after.test",
				quitDriverAfterTest);
	}
	
	/**
	 * Convenience method to get the singleton object.
	 * @return Config
	 */
	private static Config getInstance() {
		return ConfigHolder.instance;
	}
    
	/**
	 * <p>Copy an example properties file from the framework
	 * to the classloader source folder of the project this jar
	 * is imported in.</p>
	 * <p>You can simple call it from a dummy test method to collect the file.</p>
	 * @throws IOException
	 */
    public static void copyLocalExample(String absolutePath) throws IOException {
    	File sourceFile = new File(
    			Config
    			.class
    			.getResource("/selenium.properties")
    			.getFile()
    			);
    	
        File targetFile = new File(absolutePath);
        
        // Meant to run from console in test projects.
        // Hence the dreadful System.out.println in the code.
        System.out.println(
        		String.format("Example properties file copied to: %s", targetFile)
        		);

        FileUtils.forceMkdir(targetFile.getParentFile());
        FileUtils.copyFile(sourceFile, targetFile);
    }
}
