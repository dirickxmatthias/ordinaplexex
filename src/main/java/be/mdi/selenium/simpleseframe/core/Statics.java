package be.mdi.selenium.simpleseframe.core;

import be.mdi.selenium.simpleseframe.core.elements.WebElementCollection;
import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;
import be.mdi.selenium.simpleseframe.core.locator.Locator;
import be.mdi.selenium.simpleseframe.core.locator.SearchContextWaitFactory;

import static be.mdi.selenium.simpleseframe.core.CustomExpectedConditions.readyStateCompleteCondition;
import static be.mdi.selenium.simpleseframe.core.CustomExpectedConditions.readyStateInteractiveCondition;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.InvocationTargetException;

/**
 * <p>Set of static methods to enable fluent test-writing.</p>
 * @author Matthias.Dirickx
 *
 */
public class Statics {
	
    /* Driver */

	/**
	 * <p>Open a web URL in WebDriver. Which WebDriver is managed by the
	 * the setup in the {@link Config} object.</p>
	 * @param url The url you want to navigate to
	 */
	public static void open(String url) {
		Driver.goTo(url);
	}

	/**
	 * <p>Open a url in WebDriver.</p>
	 * <p>A page object is returned.</p>
	 * <p>{@code
	 * //To return an existing page object:
	 * PageObject po = new PageObject();
	 * po.someSetting(settinghere);
	 * po.someOtherSetting(someOthersetting);
	 *
	 * //now you can continue fluently starting from the page object
	 * open("http://www.example.com").f("someElement").click();}</p>
	 * @param url The url you want to navigate to
	 * @param clazz The class you want to have returned
	 * @param object The object that is returned (preconfigured class object)
	 * @param <T> Generic class
	 * @return T Class defined in clazz
	 */
	@SuppressWarnings("unchecked")
	public static <T> T open(String url, Class<T> clazz, Object object)  {
		open(url);
		return (T)object;
	}

	/**
	 * <p>Open a web url in WebDriver, and return an instance of the provided class.</p>
	 * <p>The class provided must either have no constructors ( and therefore only the
	 * default and empty constructor ), or have at least one constructor without
	 * arguments.</p>
	 * <p>All errors for reflective instantiation are swallowed and thrown as
	 * a RuntimeException.</p>
	 * @param url The url you want to navigate to
	 * @param clazz The class you want to have returned
	 * @param <T> Generic type
	 * @return T Defined class in clazz
	 */
	public static <T> T open(String url, Class<T> clazz) {
		open(url);
		try {
			return clazz.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | InvocationTargetException
				| IllegalAccessException | IllegalArgumentException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * <p>Kill and destroy all the browsers.</p>
	 * <p>This includes closing them and removing them from the thread map.</p>
	 */
	public static void destroyAllBrowsers() {
		Driver.destroyAll();
	}

	/**
	 * <p>Destroy the browser in the current thread and remove it form the
	 * thread map.</p>
	 */
	public static void destroyBrowser() {
		Driver.destroy();
	}

	/**
	 * <p>Clean the browser in the current thread.</p>
	 * <p>Cleaning includes:
	 * <ul>
	 *     <li>Delete all cookies</li>
	 *     <li>Delete the session storage</li>
	 *     <li>Delete the local storage</li>
	 * </ul></p>
	 */
	public static void cleanBrowser() {
		Driver.clean();
	}

	/**
	 * <p>Get the page title of the current web page.</p>
	 * @return String
	 */
	public static String getPageTitle() {
		return Driver.getDriver().getTitle();
	}

	/**
	 * <p>Get a FluentWait with the provided context.</p>
	 * <p>The provided context may be a WebElement or a WebDriver. Any object
	 * implementing {@link SearchContext} will be accepted.</p>
	 * @param context SearchContext (WebDriver or WebElement)
	 * @return <pre>FluentWait<SearchContext></pre>
	 */
	public static FluentWait<SearchContext> fluentWait(SearchContext context) {
		return SearchContextWaitFactory.getDefaultSearchContextWait(context);
	}

	/**
	 * <p>Return a default WebDriverWait with the polling interval and the
	 * timeout from the settings as the arguments.</p>
	 * @return WebDriverWait
	 */
	public static WebDriverWait driverWait() {
		return SearchContextWaitFactory.getDefaultWebDriverWait(Driver.getDriver());
	}

	public static void waitForReadyStateInteractive() {
		driverWait().until(readyStateInteractiveCondition);
	}

	public static void waitForReadyStateCompleted() {
		driverWait().until(readyStateCompleteCondition);
	}
	
	/* Finding elements */

	/**
	 * <p>Find elements by the {@link By} selector type.</p>
	 * @param by By
	 * @return WebElementWrapper
	 */
	public static WebElementWrapper f(By by) {
		return Locator.locateFirst(Driver.getDriver(), by);
	}

	/**
	 * <p>Find multiple elements by the {@link By} selector type.</p>
	 * @param by By
	 * @return WebElementCollection
	 */
	public static WebElementCollection ff(By by) {
		return Locator.locateAll(Driver.getDriver(), by);
	}

	/**
	 * <p>Find an element by the CSS selector with the provided string.</p>
	 * @param css String with CSS selector
	 * @return WebElementWrapper
	 */
	public static WebElementWrapper f(String css) {
		return f(By.cssSelector(css));
	}

	/**
	 * <p>Find multiple elements by the CSS selector with the provided string.</p>
	 * @param css String with CSS selector
	 * @return WebElementCollection
	 */
	public static WebElementCollection ff(String css) {
		return ff(By.cssSelector(css));
	}

	/**
	 * <p>Find an element by the XPath selector with the provided string.</p>
	 * @param xpath String with XPath selector
	 * @return WebElementWrapper
	 */
	public static WebElementWrapper fx(String xpath) {
		return f(By.xpath(xpath));
	}

	/**
	 * <p>Find multiple elements by the XPath selector with the provided string.</p>
	 *
	 * @param xpath String with XPath selector
	 * @return WebElementCollection
	 */
	public static WebElementCollection ffx(String xpath) {
		return ff(By.xpath(xpath));
	}
}
