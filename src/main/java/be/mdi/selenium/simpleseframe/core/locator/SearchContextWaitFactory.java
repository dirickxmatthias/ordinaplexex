package be.mdi.selenium.simpleseframe.core.locator;

import be.mdi.selenium.simpleseframe.core.Config;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchContextWaitFactory {
	
	public static FluentWait<SearchContext> getSearchContextWait(SearchContext context) {
		return new
				FluentWait<SearchContext>(context)
				.withTimeout(Config.getDriverFluentWaitTimeout())
				.pollingEvery(Config.getDriverFluentWaitPolling())
				.ignoring(ElementNotFoundException.class, StaleElementReferenceException.class);
	}

	public static FluentWait<SearchContext> getDefaultSearchContextWait(SearchContext context) {
		return
		new FluentWait<SearchContext>(context)
			.withTimeout(Config.getDriverFluentWaitTimeout())
			.pollingEvery(Config.getDriverFluentWaitPolling());
	}

	public static WebDriverWait getDefaultWebDriverWait(WebDriver webDriver) {
		return
		new WebDriverWait(
				webDriver,
				Config.getDriverFluentWaitTimeout().getSeconds(),
				Config.getDriverFluentWaitPolling().toMillis()
		);
	}
}
