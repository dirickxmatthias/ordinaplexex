package be.mdi.selenium.simpleseframe.core.driver;

public enum WebDriverType {
    CHROME,
    FIREFOX,
    HTML_UNIT,
    REMOTE;
}
