package be.mdi.selenium.simpleseframe.core.actions;

import be.mdi.selenium.simpleseframe.core.elements.WebElementWrapper;

import static be.mdi.selenium.simpleseframe.core.Driver.getDriver;

public class UserActions {

    public static UserAction hoverOver(WebElementWrapper webElementWrapper) {
        return new HoverUserAction(getDriver(), webElementWrapper);
    }

}
