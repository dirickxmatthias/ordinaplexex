package be.mdi.selenium.simpleseframe.core.driver;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import be.mdi.selenium.simpleseframe.core.Config;

/**
 * <p>Factory class to produce the firefox driver objects.</p>
 * <p>Factory classes like this are called by the {@link DriverFactory}.</p>
 * <p>The factory class should contain all functions to instantiate a {@link WebDriver} of type {@link FirefoxDriver}.</p>
 * 
 * @author Matthias.Dirickx
 *
 */
public class FirefoxDriverFactory {
	
	private FirefoxOptions firefoxOptions;

    public FirefoxDriverFactory() {
    	this.firefoxOptions = Config.getFirefoxOptions();
    }
	
	public FirefoxDriver buildAndGetDriver() {		
		return new FirefoxDriver(firefoxOptions);
	}
}
