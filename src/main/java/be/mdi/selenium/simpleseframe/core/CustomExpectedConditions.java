package be.mdi.selenium.simpleseframe.core;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class CustomExpectedConditions {

    public static ExpectedCondition<Boolean> readyStateCompleteCondition = readyStateCondition("complete");
    public static ExpectedCondition<Boolean> readyStateInteractiveCondition = readyStateCondition("interactive");

    private static ExpectedCondition<Boolean> readyStateCondition(String readyState) {
        return
        new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver)
                        .executeScript("return document.readyState")
                        .toString()
                        .equals(readyState);
            }
        };
    }

}
