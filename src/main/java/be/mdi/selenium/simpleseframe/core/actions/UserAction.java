package be.mdi.selenium.simpleseframe.core.actions;

public interface UserAction {
    public void execute();
}
