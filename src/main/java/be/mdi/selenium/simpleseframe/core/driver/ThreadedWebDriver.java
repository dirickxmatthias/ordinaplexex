package be.mdi.selenium.simpleseframe.core.driver;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

/**
 * <p>Singleton class that stores and serves the WebDriver objects to the requesting processes.</p>
 * 
 * @author Matthias.Dirickx
 *
 */
public class ThreadedWebDriver {
	
	/**
	 * Map of thread ID to WebDriver.
	 */
	private HashMap<Long, WebDriver> driverThreadMap;
	
	private ThreadedWebDriver() {
		driverThreadMap = new HashMap<>();
	}
	
	private static class DriverHolder {
		private static ThreadedWebDriver instance = new ThreadedWebDriver();
	}
	
	private static HashMap<Long, WebDriver> getDriverThreadMap() {
		return DriverHolder.instance.driverThreadMap;
	}
	
	/**
	 * <p>Get a WebDriver object. The WebDriver object linked to the calling thread is returned.</p>
	 * <p>If the WebDriver object does not exist for the thread, then it will be created.</p>
	 * @return {@link WebDriver}
	 */
	public static WebDriver getDriver() {
		Long threadId = Thread.currentThread().getId();
		
		if(!getDriverThreadMap().keySet().contains(threadId)) {
			getDriverThreadMap().put(threadId, DriverFactory.getDriver());
		}
		
		return DriverHolder.instance.driverThreadMap.get(threadId);
	}
	
	/**
	 * <p>Destroy the WebDriver for the calling thread.</p>
	 * <p>The {@link WebDriver#quit()} method will be called on the driver object.<p>
	 * <p>After that the entry for the tread will be removed from the thread.</p>
	 */
	public static void destroyDriver() {
		destroyDriver(Thread.currentThread().getId());
	}
	
	/**
	 * <p>Destroy all of the WebDrivers for all threads.</p>
	 * <p>The {@link WebDriver#quit()} method will be called on each driver object.<p>
	 * <p>After that the all entries for the treads will be removed from the map.</p>
	 */
	public static void destroyAllDrivers() {
		for (Long threadId : getDriverThreadMap().keySet()) {
			destroyDriver(threadId);
		}
	}
	
	private static void destroyDriver(Long threadId) {
		if(!(getDriverThreadMap().get(threadId) == null)) {
			getDriverThreadMap().get(threadId).quit();
		}
		getDriverThreadMap().remove(threadId);
	}
	
	/**
	 * <p>Destroy all drivers, and reset the HashMap holding the WebDriver objects.</p>
	 */
	public static void reset() {
		destroyAllDrivers();
		DriverHolder.instance.driverThreadMap = new HashMap<Long, WebDriver>();
	}
}
