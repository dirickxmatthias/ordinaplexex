package be.mdi.selenium.simpleseframe.core.elements;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * <p>The WebElementCollection class represents the {@code List<WebElement>} that is returned from 
 * the {@link WebElement#findElements(By)}, or the similar function for WebDriver.</p>
 * 
 * @author Matthias.Dirickx
 *
 */
public class WebElementCollection implements List<WebElementWrapper> {

	private WebElementWrapper parent;
	private By by;
	private List<WebElement> seleniumWebElementList;
	private List<WebElementWrapper> webElementWrapperList;
	
	/**
	 * <p>Initialize a WebElementCollection with a list of WebElement objects.</p>
	 * <p>The By is stored for logging purposes.</p>
	 * <p>See {@link #WebElementCollection(List, By, WebElementWrapper)} for more details.</p>
	 * @param seleniumWebElementList - {@code List<WebElement>}
	 * @param by - By
	 */
	public WebElementCollection(List<WebElement> seleniumWebElementList, By by) {
		initialize(seleniumWebElementList, by, null);
	}
	
	/**
	 * <p>Initialize a WebElementCollection with a list of WebElement objects.</p>
	 * <p>The webelements list is stored both raw and as a list of WebElementWrapper objects.</p>
	 * <p>The WebElementWrapper parent element is stored for reference.</p>
	 * @param seleniumWebElementList {@code List<WebElement>}
	 * @param by By
	 * @param parent WebElementWrapper
	 */
	public WebElementCollection(List<WebElement> seleniumWebElementList, By by, WebElementWrapper parent) {
		initialize(seleniumWebElementList, by, parent);
	}
	
	public List<WebElement> getWebElementList() {
		return this.seleniumWebElementList;
	}
	
	private void initialize(List<WebElement> seleniumWebElementList, By by, WebElementWrapper parent) {
		this.seleniumWebElementList = seleniumWebElementList;
		this.webElementWrapperList = getWebElementWrapperList(seleniumWebElementList);
		this.by = by;
		this.parent = parent;
	}
	
	private List<WebElementWrapper> getWebElementWrapperList(List<WebElement> seleniumWebElementList) {
		List<WebElementWrapper> webElementWrapperList = new ArrayList<>();
		int listSize = seleniumWebElementList.size();
		for(int i = 0; i < listSize; i++) {
			webElementWrapperList.add(new WebElementWrapper(seleniumWebElementList.get(i), by, parent, i));
		}
		return webElementWrapperList;
	}
	
	/* List implementation */
	
	@Override
	public int size() {
		return webElementWrapperList.size();
	}

	@Override
	public boolean isEmpty() {
		return webElementWrapperList.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return webElementWrapperList.contains(o);
	}

	@Override
	public Iterator<WebElementWrapper> iterator() {
		return webElementWrapperList.iterator();
	}

	@Override
	public Object[] toArray() {
		return webElementWrapperList.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return webElementWrapperList.toArray(a);
	}

	@Override
	public boolean add(WebElementWrapper e) {
		return webElementWrapperList.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return webElementWrapperList.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return webElementWrapperList.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends WebElementWrapper> c) {
		return webElementWrapperList.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends WebElementWrapper> c) {
		return webElementWrapperList.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return webElementWrapperList.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return webElementWrapperList.retainAll(c);
	}

	@Override
	public void clear() {
		webElementWrapperList.clear();
	}

	@Override
	public WebElementWrapper get(int index) {
		return webElementWrapperList.get(index);
	}

	@Override
	public WebElementWrapper set(int index, WebElementWrapper element) {
		return webElementWrapperList.set(index, element);
	}

	@Override
	public void add(int index, WebElementWrapper element) {
		webElementWrapperList.add(index, element);
	}

	@Override
	public WebElementWrapper remove(int index) {
		return webElementWrapperList.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return webElementWrapperList.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return webElementWrapperList.lastIndexOf(o);
	}

	@Override
	public ListIterator<WebElementWrapper> listIterator() {
		return webElementWrapperList.listIterator();
	}

	@Override
	public ListIterator<WebElementWrapper> listIterator(int index) {
		return webElementWrapperList.listIterator(index);
	}

	@Override
	public List<WebElementWrapper> subList(int fromIndex, int toIndex) {
		return webElementWrapperList.subList(fromIndex, toIndex);
	}
}
