package be.mdi.selenium.simpleseframe.core.proxy;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.HashMap;

import org.openqa.selenium.Proxy;

import be.mdi.selenium.simpleseframe.core.Config;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;

public class SeleniumProxy {
	
	private BrowserMobProxy proxy;
	private Proxy seleniumProxy;
	
	private SeleniumProxy(HashMap<String, String> proxyHeaders, Integer port) {
		createProxy(proxyHeaders, port);
	}
	
	private static class rspHolder {
		private static SeleniumProxy instance = new SeleniumProxy(Config.getProxyHeaders(), 12345);
	}
	
	private void createProxy(HashMap<String, String> headers, Integer port) {
		proxy = new BrowserMobProxyServer();
		proxy.addHeaders(headers);
		proxy.start();
		
		createSeleniumProxy();
	}
	
	private void createSeleniumProxy() {
		String host = "127.0.0.1";
		
		try {
			host = Inet4Address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
        String seleniumProxyString = String.format("%s:%s", host, proxy.getPort());
		
		seleniumProxy = ClientUtil.createSeleniumProxy(proxy)
				.setHttpProxy(seleniumProxyString)
				.setSslProxy(seleniumProxyString);
	}
	
	public static Proxy getProxyForCapabilities() {		
		return rspHolder.instance.seleniumProxy;
	}
	
	public static BrowserMobProxy getBrowserMobProxy() {
		return rspHolder.instance.proxy;
	}
	
	public static void stop() {
		rspHolder.instance.proxy.stop();
	}

}
