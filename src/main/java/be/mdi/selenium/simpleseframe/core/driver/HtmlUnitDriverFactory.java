package be.mdi.selenium.simpleseframe.core.driver;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import be.mdi.selenium.simpleseframe.core.Config;

/**
 * <p>Factory class to produce the HTML unit driver objects.</p>
 * <p>Factory classes like this are called by the {@link DriverFactory}.</p>
 * <p>The factory class should contain all functions to instantiate a {@link WebDriver} of type {@link HtmlUnitDriver}.</p>
 * 
 * @author Matthias.Dirickx
 *
 */
public class HtmlUnitDriverFactory {
	
    private DesiredCapabilities caps;
	
	public HtmlUnitDriverFactory() {
		this.caps = Config.getDesiredCapabilities();
		caps.setBrowserName("htmlunit");
	}
	
	public HtmlUnitDriver buildAndGetDriver() {
		return new HtmlUnitDriver(caps);
	}

}
