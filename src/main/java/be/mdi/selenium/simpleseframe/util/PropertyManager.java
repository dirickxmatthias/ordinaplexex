/**
 * https://gist.github.com/matthias-dirickx/35017c32cfd8360754327ccbfe34c86f
 * 
 * <p>Static property manager.</p>
 * <p>The rudimentary property manager loads a file from resources (root).</p>
 * <p>Name : rrntest.properties</p>
 *
 * <p>The properties are loaded to a map and resolved.</p>
 *
 * <p>If any properties are added at runtime you need to call resolveProperties() if the properties contain placeholders.</p>
 *
 * <p>A placeholder is defined with start '${' and end '}'. Example: ${this is a placeholder}.</p>
 *
 * <p>The placeholders may be nested, and will be resolved from inner to outer. A placeholder may not contain any of '{' or '}'.
 * For example: ${some_text{here} will not be recognized as a placeholder.
 * A placeholder containing '}' will be cut short. As an example: for ${some_text}here} will be resolved with ${some_text}
 * as the placeholder (looking for a variable named 'some_text') to replace it.</p>
 *
 * <p>The reason for this explicit call to resolveProperties is to allow
 * to add properties in an arbitry order and to resolve them only when they're all there.</p>
 * 
 * <p>Not that this class will fail silently when there is no properties file found called 'rrntest.properties'.
 * The reason is that it is not strictly necessary to have properties file. You can use this to set properties
 * in an application context that may be consulted by different class instances in that context.</p>
 *
 * <p>There are three public accessors.</br>
 * <ul>
 *   <li>getProperty()</li>
 *   <li>setProperty()</li>
 *   <li>resolveProperties()</li>
 * </ul>
 * </p>
 *
 * <p>If there are properties containing placeholders,
 * make sure to call resolveProperties() to resolve all of the property placeholders.</p>
 * 
 * @author Matthias.Dirickx
 *
 */
package be.mdi.selenium.simpleseframe.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PropertyManager {
	
	public static String propertyFile;

    private static final String PROP_FIND_REGEX = "\\$\\{[^\\{\\}]*\\}";
    private static final Pattern pattern = Pattern.compile(PROP_FIND_REGEX);

    private static PropertyManager pr;
    private Properties props;
    private Map<String, String> propsMap;

    private PropertyManager() {
    	
        props = new Properties();
        
        try {
        	
        	InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(propertyFile);
        	
            props.load(inputStream);
            
        } catch(IOException e) {
            e.printStackTrace();
        } catch(NullPointerException e) {
        	e.printStackTrace();
        }
        
        propsMap = new HashMap<String, String>();
    }

    private void pushPropsToMap() {
        for(String key : props.stringPropertyNames()) {
            propsMap.put(
            		key,
                    props.getProperty(key)
            );
        }
    }

    /**
     * <p>Recursive function to resolve the property placeholders.</p>
     * @param propValue
     * @return String (resolved property without placeholders)
     */
    private static String resolveProperty(String propValue) {
        String resolvedProperty = propValue;
        Matcher matcher = pattern.matcher(propValue);

        if(matcher.find(0)) {

            String match = matcher.group();
            resolvedProperty = resolvedProperty.replace(
            		match,
            		getProperty(match.substring(2, match.length()-1)));
            Matcher nestedMatcher = pattern.matcher(resolvedProperty);

            if(nestedMatcher.find(0)) {
                resolvedProperty = resolveProperty(resolvedProperty);
            }
        }
        return resolvedProperty;
    }

    private static synchronized Map<String, String> getProperties() {
        if(pr == null) {
            synchronized (PropertyManager.class) {
                if(pr == null) {
                    pr = new PropertyManager();
                    pr.pushPropsToMap();
                    resolveProperties();
                }
            }
        }
        return pr.propsMap;
    }
    
    private void reset() {
    	pr = null;
    }
    
    public static void loadFile(String propertyFile) {
    	pr.reset();
    	PropertyManager.propertyFile = propertyFile;
    }

    /**
     * <p>Gets a property from the HashMap stored in the PropertyManager singleton.</p>
     * <p>There is a pecking order in getting he property:
     * <ol>
     * <li>System property named {@code testingtp.the.property.name} (testingtp is prepended to avoid collision with other properties)</li>
     * <li>Property as defined in the given properties file. (here rrntest.properties)</li>
     * </ol></p>
     * 
     * @param name
     * @return String
     */
    public static String getProperty(String name) {
    	if(System.getProperty("testingtp." + name) != null) {
    		return System.getProperty("testingtp." + name);
    	} else {
    		return getProperties().get(name);
    	}
    }
    
    /**
     * <p>Gets a property from the HashMap stored in the PropertyManager singleton.
     * When the property is not found, it returns the defined default value.</p>
     * <p>There is a pecking order in getting the properties:
     * <ol>
     * <li>System property named {@code testingtp.the.property.name} (testingtp is prepended to avoid collision with other properties)</li>
     * <li>Property as defined in the given properties file. (here rrntest.properties)</li>
     * <li>The default value that is passed to the method as a parametre.
     * </ol></p>
     * @param name (String, name of the property)
     * @param defaultValue (String, the default value to return if the property is not found)
     * @return String (Either the default value or the property value)
     */
    public static String getProperty(String name, String defaultValue) {
    	if(getProperty(name) != null) {
    		return getProperty(name);
    	}
    	return defaultValue;
    }
    
    /**
     * <p>Get a property frmo the HashMap stored in the PropertyManager singleton as a boolean value.
     * This exists to make usage in if statements easier.</p>
     * <p>When the property does not exist, the default value is returned.</p>
     * <p>If the key exists, then any other value then 'true' will return false.</p>
     * 
     * @param name
     * @param defaultValue
     * @return
     */
    public static boolean getBooleanProperty(String name, boolean defaultValue) {
    	if(getProperty(name) != null) {
    		String value = getProperty(name);
    		return value.equalsIgnoreCase("true");
    	}
		return defaultValue;
	}
    
    public static Long getLongProperty(String name, Long defaultValue) {
    	if(getProperty(name) != null) {
    		String value = getProperty(name);
    		return Long.valueOf(value);
    	}
    	return defaultValue;
    }

    /**
     * <p>Set property  in the HashMap stored in the PropertyManager singleton.
     * Setting a value with setProperty does NOT store this property in the .properties file.</p>
     * @param name
     * @param value
     */
    public static void setProperty(String name, String value) {
        getProperties().put(name, value);
    }

    /**
     * <p>Resolve all properties that are stored in the hashmap in the PropertyManager singleton.</p>
     */
    public static void resolveProperties() {
        for(String key : getProperties().keySet()) {
            String resolvedProp = resolveProperty(getProperty(key));
            setProperty(key, resolvedProp);
        }
    }
}
